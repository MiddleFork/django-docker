from debian:stable

RUN apt update && apt install -y \
  python3 python3-pip \
  && update-alternatives --install /usr/bin/python python /usr/bin/python3
  
  