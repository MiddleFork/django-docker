import os
import sys


sys.path.append(os.path.dirname(os.path.abspath(__file__)))  # so we can grab components of settings from cwd

"""
Original settings created by django-admin startproject. 
Rather than attempting to refactor the default to suit our needs, we 
Over-ride any of them or build on them.  This is expecially helpful, for example
in allowing us to use the generated SECRET_KEY without having to do so ourselves.
"""

from settings_orig import *
from settings_rest import *

DEBUG = True

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
INSTALLED_APPS += REST_APPS

if DEBUG:
    INSTALLED_APPS += ['debug_toolbar']


ROOT_URLCONF = 'drf.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = '/static/'


